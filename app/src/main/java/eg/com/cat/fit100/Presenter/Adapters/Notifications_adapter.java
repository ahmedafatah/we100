package eg.com.cat.fit100.Presenter.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eg.com.cat.fit100.Model.Notifications_item;
import eg.com.cat.fit100.R;

public class Notifications_adapter extends RecyclerView.Adapter<Notifications_adapter.Notifications_adapterHolder> {

    Notifications_item foodResonseModel;
    private Context context;

    public Notifications_adapter(Notifications_item foodResonseModel, Context context) {
        this.foodResonseModel = foodResonseModel;
        this.context = context;
    }


    @NonNull
    @Override
    public Notifications_adapter.Notifications_adapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Notifications_adapter.Notifications_adapterHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_notification_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Notifications_adapter.Notifications_adapterHolder holder, int position) {

        holder.offerTitle.setText(foodResonseModel.getData().get(position).getNotifiable().getTitle());
        holder.offerdescription.setText(foodResonseModel.getData().get(position).getNotifiable().getBody());
        holder.tvDate.setText(foodResonseModel.getData().get(position).getUpdated_at());


//        Picasso.get().load(MAIN_PICTURES_URL +
//                foodResonseModel.getDATA().get(position).getPhotos().get(0).getPath() )
//                .fit()
//                .centerCrop()
//                .into(holder.offerImage);


    }

    @Override
    public int getItemCount() {
        return foodResonseModel.getData().size();
    }

    public class Notifications_adapterHolder extends RecyclerView.ViewHolder {

        TextView offerTitle, offerdescription, tvDate;


        public Notifications_adapterHolder(View itemView) {
            super(itemView);

            offerTitle = itemView.findViewById(R.id.tvTitle);
            offerdescription = itemView.findViewById(R.id.tvContent);
            tvDate = itemView.findViewById(R.id.tvDate);

        }
    }

}
