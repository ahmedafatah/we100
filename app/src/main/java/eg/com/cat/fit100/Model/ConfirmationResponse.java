package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConfirmationResponse {

    @SerializedName("data")
    public List<Data> data;
    @SerializedName("state")
    public String state;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public class Data {
        @SerializedName("id")
        public int id;
        @SerializedName("title")
        public String title;
        @SerializedName("schedule_id")
        public String brief;
        @SerializedName("body")
        public String body;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBrief() {
            return brief;
        }

        public void setBrief(String brief) {
            this.brief = brief;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }
}
