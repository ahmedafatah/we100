package eg.com.cat.fit100.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.juanpabloprado.countrypicker.CountryPicker;
import com.juanpabloprado.countrypicker.CountryPickerListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import eg.com.cat.fit100.Model.LoginResponse;
import eg.com.cat.fit100.Model.RegisterResponseModel;
import eg.com.cat.fit100.Model.programsResponseModel;
import eg.com.cat.fit100.Presenter.FileUtils;
import eg.com.cat.fit100.Presenter.ServiceGenerator;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.MainActivity;
import eg.com.cat.fit100.WebServices.Api;
import eg.com.cat.fit100.WebServices.Webservice;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static eg.com.cat.fit100.View.MainActivity.currentuser;
import static eg.com.cat.fit100.WebServices.Services.MAIN_PICTURES_URL;


public class EditProfileFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    Spinner dropdown;
    ImageView picimage, bck;
    TextView country_name;
    String country;
    ProgressDialog progress;
    EditText input_username, input_name, input_email, weight, height;
    Button button_reset_password, button_logout, button_update_info;
    RadioGroup genderRadioGroup;
    RadioButton maleRadioButton, femaleRadioButton;
    LinearLayout country_picker_search;
    CountryPicker picker;
    String[] items, itemsIDs;
    String programId;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private String gender;
    private Uri uri;


    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        picimage = view.findViewById(R.id.picimage);
        // editImage = view.findViewById(R.id.imagetext);
        input_username = view.findViewById(R.id.input_username);
        button_reset_password = view.findViewById(R.id.button_reset_password);
        dropdown = view.findViewById(R.id.spinner1);

        input_name = view.findViewById(R.id.input_name);
        input_email = view.findViewById(R.id.input_email);
        button_reset_password = view.findViewById(R.id.button_reset_password);
        button_logout = view.findViewById(R.id.button_logout);
        button_update_info = view.findViewById(R.id.button_update_info);
        weight = view.findViewById(R.id.input_weight);
        height = view.findViewById(R.id.input_height);
        bck = view.findViewById(R.id.bck);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        genderRadioGroup = (RadioGroup) view.findViewById(R.id.genderRadioGroup);


        if (currentuser.getPhotos() != null && currentuser.getPhotos().size() != 0) {
            if (currentuser.getPhotos().get(0).getPath() != null) {
                Picasso.get().load(MAIN_PICTURES_URL +
                        currentuser.getPhotos().get(0).getPath())
                        .fit()
                        //       .centerInside()
                        .noFade()
                        .into(picimage);
            }
        }


        country_picker_search = view.findViewById(R.id.country_picker_search);
        country_name = view.findViewById(R.id.country_name);

        genderRadioGroup = (RadioGroup) view.findViewById(R.id.genderRadioGroup);

        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.maleRadioButton: {
                        gender = "1";
                        break;
                    }
                    case R.id.femaleRadioButton: {
                        gender = "0";
                        break;
                    }
                }
            }
        });

        picimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (EasyPermissions.hasPermissions(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                    openGalleryIntent.setType("image/*");
                    startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
                } else {
                    // Do not have permissions, request them now
                    EasyPermissions.requestPermissions(getActivity(), "Gallery Permission",
                            READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                }


            }
        });

        bck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();

            }
        });


        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_logout.startAnimation(buttonClick);

                gotologin();
            }
        });


        country_picker_search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setCountry();
            }
        });


        button_update_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_update_info.startAnimation(buttonClick);

                updateInfo();
            }
        });

        //create a list of items for the spinner.
        getDataFromServer();

        setData();

        return view;
    }

    public void setCountry() {
        picker = CountryPicker.getInstance("Select Country", new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code) {
                //   Toast.makeText(getActivity(), "Name: " + name, Toast.LENGTH_SHORT).show();
                country = name;
                country_name.setText(country);
                DialogFragment dialogFragment =
                        (DialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag("CountryPicker");
                dialogFragment.dismiss();
            }
        });
        picker.show(getActivity().getSupportFragmentManager(), "CountryPicker");
    }


    private void updateInfo() {

        progress.show();
        updateUserData();
        updateUserProgram();
        progress.dismiss();

    }

    private void updateUserProgram() {
        HashMap<String, String> data = new HashMap<>();

        data.put("user_id", currentuser.getId());
        data.put("program_id", programId);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        Response<LoginResponse> response = null;
        try {
            response = Webservice.getInstance().getApi().change_program(data).execute();

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!response.isSuccessful()) {
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getContext(), "Program Updated successfully", Toast.LENGTH_LONG).show();
            currentuser.setProgram_id(programId);
        }

    }

    private void updateUserData() {

        HashMap<String, String> user = new HashMap<>();

        user.put("user_id", currentuser.getId());
        user.put("name", input_name.getText().toString().trim());
        user.put("email", input_email.getText().toString().trim());
        user.put("weight", weight.getText().toString().trim());
        user.put("height", height.getText().toString().trim());
        user.put("gender", gender);
        user.put("country", country);


        Webservice.getInstance().getApi().update_user(user).enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getActivity(), "Successfull.", Toast.LENGTH_LONG).show();
                 //   currentuser = response.body().getData();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void setData() {

        input_username.setText(currentuser.getUsername());
        if (currentuser.getName().equals("") || currentuser.getName() == null) {
            input_name.setText(currentuser.getUsername());

        } else {
            input_name.setText(currentuser.getName());

        }
        input_email.setText(currentuser.getEmail());
        height.setText(currentuser.getHeight());
        weight.setText(currentuser.getWeight());

        if (currentuser.getGender().equals("1"))
            genderRadioGroup.check(R.id.maleRadioButton);
        else
            genderRadioGroup.check(R.id.femaleRadioButton);

        country_name.setText(currentuser.getCountry());
        country = currentuser.getCountry();

    }

    private void gotologin() {
        FragmentManager manager = getActivity().getFragmentManager();

        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        gotoHome();
    }

    private void gotoHome() {

       /* try {
            Intent k = new Intent(getActivity(), NavigationActivity.class);
            startActivity(k);
        } catch(Exception e) {
            e.printStackTrace();
        }*/

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .commit();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void uploadFile(Uri fileUri) {

        progress.show();


        // create upload service client
        Api service =
                ServiceGenerator.createService(Api.class);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(getContext(), fileUri);

        //   File imagefile = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() ,file.getAbsolutePath() );

        final Bitmap selectedImage = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 30, stream);
        byte[] byteArray = stream.toByteArray();
        String strBase64 = Base64.encodeToString(byteArray, 0);

        // strBase64

        HashMap<String, String> data = new HashMap<>();

        data.put("user_id", currentuser.getId());
        data.put("image", strBase64);

        Call<Object> call = service.update_user_image(data);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call,
                                   Response<Object> response) {
                Log.v("Upload", "success");
                Toast.makeText(getActivity(), "picture uploaded successfully", Toast.LENGTH_LONG).show();
                picimage.setImageBitmap(selectedImage);


                 SharedPreferences loginPreferences;

                loginPreferences = getActivity().getSharedPreferences("loginPrefs", MODE_PRIVATE);

                login(loginPreferences.getString("username", "") , loginPreferences.getString("password", ""));

                progress.dismiss();

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                Toast.makeText(getActivity(), "Check internet", Toast.LENGTH_LONG).show();
                progress.dismiss();

            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            uri = data.getData();
            uploadFile(uri);
            progress.show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

     //   ((TextView) v).setTextColor(Color.WHITE);

        Toast.makeText(getContext(), "You have selected " + items[+position], Toast.LENGTH_LONG).show();

        // programId = String.valueOf(position + 1);

        programId = String.valueOf(itemsIDs[+position]);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void getDataFromServer() {

        progress.show();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        Response<programsResponseModel> response = null;
        try {
            response = Webservice.getInstance().getApi().programs().execute();


        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!response.isSuccessful()) {
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), "Data Get Successfully", Toast.LENGTH_LONG).show();
            items = new String[response.body().getData().size()];
            itemsIDs = new String[response.body().getData().size()];
            for (int i = 0; i < response.body().getData().size(); ++i) {
                items[i] = response.body().getData().get(i).getTitle();
                itemsIDs[i] = (response.body().getData().get(i).getId());
            }

            //create an adapter to describe how the items are displayed, adapters are used in several places in android.
            //There are multiple variations of this, but this is the basic variant.
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
            //set the spinners adapter to the previously created one.
            dropdown.setAdapter(adapter);

        }

        dropdown.setOnItemSelectedListener(this);

        progress.dismiss();

    }


    public void login(String user, final String pass) {

        final HashMap<String, String> data = new HashMap<>();

        data.put("email", user);
        data.put("password", pass);

            Webservice.getInstance().getApi().Login(data).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }

                    } else {


                        final int userId = response.body().getData().id;
                        final String userIdString = Integer.toString(userId);

                        currentuser.setName(response.body().data.name);
                        currentuser.setCountry(response.body().data.country);
                        currentuser.setId(String.valueOf(response.body().data.id));
                        currentuser.setUsername(response.body().data.username);
                        currentuser.setEmail(response.body().data.email);
                        currentuser.setGender(response.body().data.gender);
                        currentuser.setHeight(response.body().data.height);
                        currentuser.setWeight(response.body().data.weight);
                        currentuser.setBmi(response.body().data.bmi);
                        currentuser.setPhotos(response.body().data.photos);


                        SharedPreferences mPrefs = getActivity().getPreferences(MODE_PRIVATE);

                        SharedPreferences.Editor prefsEditor = mPrefs.edit();
                        Gson gson = new Gson();
                        String json = gson.toJson(currentuser); // myObject - instance of MyObject
                        prefsEditor.putString("MyObject", json);
                        prefsEditor.commit();





                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                    Log.e("login", "onFailure: ", t);

                }
            });

        }


}
