package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;

import eg.com.cat.fit100.Model.RegisterResponseModel;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgetPasswordFragment extends Fragment {

    EditText email;
    Button reset;
    ProgressDialog progress;

    public ForgetPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forget_password, container, false);

        email = (EditText) view.findViewById(R.id.input_email);
        reset = (Button) view.findViewById(R.id.btn_reset);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog


        reset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                reset(email.getText().toString().trim());
            }
        });


        return view;
    }

    private void reset(String email) {

        if (email.isEmpty())
            Toast.makeText(getActivity(), "Please enter your credentials correctly ", Toast.LENGTH_LONG).show();

        else {

            progress.show();


            HashMap<String, String> data = new HashMap<>();

            data.put("email", email);


            Webservice.getInstance().getApi().Reset_Password(data).enqueue(new Callback<RegisterResponseModel>() {
                @Override
                public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                    if (response.isSuccessful()) {
                        progress.dismiss();
                        Toast.makeText(getActivity(), response.body().getData().toString(), Toast.LENGTH_LONG).show();

                    } else {
                        progress.dismiss();

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }


                    }
                }

                @Override
                public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                    Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                    Log.e("login", "onFailure: ", t);
                }
            });
        }

    }

}
