package eg.com.cat.fit100.Presenter.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import eg.com.cat.fit100.Model.OffersResponseModel;
import eg.com.cat.fit100.R;

import static eg.com.cat.fit100.WebServices.Services.MAIN_PICTURES_URL;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsHolder> {

    OffersResponseModel offersResponseModel;

    private Context context;


    public EventsAdapter(OffersResponseModel offersResponseModel, Context context) {
        this.offersResponseModel = offersResponseModel;
        this.context = context;
    }

    @NonNull
    @Override
    public EventsAdapter.EventsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EventsAdapter.EventsHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_layout_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EventsAdapter.EventsHolder holder, int position) {


        //    holder.main_layout.setBackgroundResource(myImageList[position % 7]);

        holder.offerTitle.setText(offersResponseModel.getData().get(position).getTitle().trim());
        //holder.offerImage.setText(offersResponseModel.getData().get(position).getTitle());
        Spanned spanned = Html.fromHtml(offersResponseModel.getData().get(position).getBody());

        String formattedText = spanned.toString();

        holder.offerdescription.setText(formattedText);

        if (offersResponseModel.getData().get(position).getPhotos() != null &&
                offersResponseModel.getData().get(position).getPhotos().size() != 0
                )
            if (offersResponseModel.getData().get(position).getPhotos().get(0).getPath() != null) {
                Picasso.get().load(MAIN_PICTURES_URL +
                        offersResponseModel.getData().get(position).getPhotos().get(0).getPath())
//                        .fit()
                        // .centerInside()
                        .into(holder.offerImage);
            }
    }


    @Override
    public int getItemCount() {
        return offersResponseModel.getData().size();
    }

    public class EventsHolder extends RecyclerView.ViewHolder {

        TextView offerTitle, offerdescription;
        ImageView moreinfooffers;
        RelativeLayout main_layout;
        View view;
        ImageView offerImage;

        public EventsHolder(View itemView) {
            super(itemView);

            offerTitle = (TextView) itemView.findViewById(R.id.offerTitle);
            offerdescription = (TextView) itemView.findViewById(R.id.offerdescription);
            // moreinfooffers = itemView.findViewById(R.id.moreinfooffers);
            offerImage = itemView.findViewById(R.id.offerImage);
            main_layout = itemView.findViewById(R.id.main_layout);

         /*   moreinfooffers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), "no more info ", Toast.LENGTH_LONG).show();
                }
            });*/

        }
    }
}
