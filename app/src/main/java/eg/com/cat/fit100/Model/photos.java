package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

public class photos {

    @SerializedName("id")
    public String id;
    @SerializedName("path")
    public String path;
    @SerializedName("imageable_id")
    public String imageable_id;
    @SerializedName("imageable_type")
    public String imageable_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getImageable_id() {
        return imageable_id;
    }

    public void setImageable_id(String imageable_id) {
        this.imageable_id = imageable_id;
    }

    public String getImageable_type() {
        return imageable_type;
    }

    public void setImageable_type(String imageable_type) {
        this.imageable_type = imageable_type;
    }


    public photos(String id, String path) {
        this.id = id;
        this.path = path;
    }
}
