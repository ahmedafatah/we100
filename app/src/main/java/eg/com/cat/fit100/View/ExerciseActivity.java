package eg.com.cat.fit100.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import eg.com.cat.fit100.Model.ImageModel;
import eg.com.cat.fit100.Model.PostsResponseModel;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.Fragments.ExerciseDetailsFragment;

public class ExerciseActivity extends AppCompatActivity {
    private static ViewPager healthTipsPager, postsPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Button complete_button;
    ImageView bck;
    CirclePageIndicator indicator, postsIndicator;
    ImageView activity, exercise, btn_newsfeed, home, offers, events, notification, btn_edit_profile, myprofilepicture;
    EditText addpost_edittext;
    Button sendpost;
    PostsResponseModel postsResponseModel;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private ArrayList<ImageModel> imageModelArrayList;
    private int[] myImageList = new int[]{R.drawable.s10_02, R.drawable.s10_02,
            R.drawable.s10_02, R.drawable.s10_02};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        setButtons();
        init();


    }

    private void init() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.exercisecontainer, new ExerciseDetailsFragment())
                .commit();
    }


    private void setButtons() {

        complete_button = findViewById(R.id.complete_button);
        bck = findViewById(R.id.bck);


        complete_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                complete_button.startAnimation(buttonClick);

                ExerciseDetailsFragment.updateUserProgram();
            }
        });


        bck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ExerciseActivity.super.onBackPressed();

            }
        });


        home = findViewById(R.id.btn_home);
        offers = findViewById(R.id.btn_offers);
        events = findViewById(R.id.btn_events);
        notification = findViewById(R.id.btn_notification);
        btn_newsfeed = findViewById(R.id.btn_newsfeed);


        btn_newsfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExerciseActivity.this, MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 3);
                startActivity(intent);
            }
        });

        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExerciseActivity.this, MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 1);
                startActivity(intent);
            }
        });

        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExerciseActivity.this, MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 2);
                startActivity(intent);
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExerciseActivity.this, MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 4);
                startActivity(intent);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExerciseActivity.this, MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 0);
                startActivity(intent);
            }
        });
    }

}
