package eg.com.cat.fit100.View.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import eg.com.cat.fit100.R;


public class ActivityChooser extends Fragment implements AdapterView.OnItemSelectedListener {

    public static int programId;
    public static int MELT_JOGGING = 7;
    public static int MELT_SWIMMING = 8;
    public static int MELT_LOAD_CARRYING = 9;
    public static int MELT_RUNNING = 9;
    public static int MELT_Weightlifting = 3;
    Spinner spinner1;
    Button button;
    String[] items = {"JOGGING", "SWIMMING", "LOAD CARRYING", "RUNNING", "Weightlifting"};

    public ActivityChooser() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activity_chooser, container, false);

        spinner1 = view.findViewById(R.id.spinner1);
        button = view.findViewById(R.id.btn_continue);

        //create an adapter to describe how the items are displayed, adapters are used in several places in android.
        //There are multiple variations of this, but this is the basic variant.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
        //set the spinners adapter to the previously created one.
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activitycontainer, new ActivityTracking())
                        .commit();
            }
        });


        return view;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        ((TextView) v).setTextColor(Color.WHITE);

        Toast.makeText(getContext(), "You have selected " + items[position], Toast.LENGTH_LONG).show();

        if (position == 0)
            programId = 7;
        else if (position == 1)
            programId = 8;
        else if (position == 2)
            programId = 9;
        else if (position == 3)
            programId = 9;
        else if (position == 4)
            programId = 3;

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        programId = 7;

    }

}
