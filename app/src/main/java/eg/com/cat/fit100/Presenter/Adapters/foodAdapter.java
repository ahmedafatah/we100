package eg.com.cat.fit100.Presenter.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import eg.com.cat.fit100.Model.CurrentDayResponseModel;
import eg.com.cat.fit100.R;

public class foodAdapter extends RecyclerView.Adapter<foodAdapter.foodHolder> {

    CurrentDayResponseModel foodResonseModel;
    private Context context;

    public foodAdapter(CurrentDayResponseModel foodResonseModel, Context context) {
        this.foodResonseModel = foodResonseModel;
        this.context = context;
    }


    @NonNull
    @Override
    public foodAdapter.foodHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new foodAdapter.foodHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_food_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull foodAdapter.foodHolder holder, int position) {

        holder.offerTitle.setText(foodResonseModel.getData().get(0).getMeals().get(position).getTitle());
        holder.offerdescription.setText(foodResonseModel.getData().get(0).getMeals().get(position).getBody());


//        Picasso.get().load(MAIN_PICTURES_URL +
//                foodResonseModel.getDATA().get(position).getPhotos().get(0).getPath() )
//                .fit()
//                .centerCrop()
//                .into(holder.offerImage);


    }

    @Override
    public int getItemCount() {
        return foodResonseModel.getData().get(0).getMeals().size();
    }

    public class foodHolder extends RecyclerView.ViewHolder {

        TextView offerTitle, offerdescription;
        ImageView moreinfooffers, offerImage;
        LinearLayout Listen;
        View view;

        public foodHolder(View itemView) {
            super(itemView);

            offerTitle = (TextView) itemView.findViewById(R.id.offerTitle);
            offerdescription = (TextView) itemView.findViewById(R.id.offerdescription);
            //  moreinfooffers = itemView.findViewById(R.id.moreinfooffers);
            // offerImage = itemView.findViewById(R.id.offerImage);

           /* moreinfooffers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext() , "no more info " , Toast.LENGTH_LONG).show();
                }
            });*/

        }
    }

}
