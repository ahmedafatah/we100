package eg.com.cat.fit100.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import eg.com.cat.fit100.R;

import static com.bumptech.glide.request.RequestOptions.diskCacheStrategyOf;
import static eg.com.cat.fit100.View.Fragments.ActivityChooser.programId;
import static eg.com.cat.fit100.View.MainActivity.currentuser;


public class ActivityTracking extends Fragment {

    private static DecimalFormat df2 = new DecimalFormat(".##");
    double res = 0;
    int hours;
    private int seconds = 0;
    private boolean startRun;
    private ImageView donutProgress;
    TextView txt_cals;
  //  GifDrawable gifFromResource;
    private Button calcualteCalories, btn_start, btn_stop;
    Context context;

    TextView tv_hours , tv_mints , tv_secs;
    public ActivityTracking() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activity_tracking, container, false);

        context = getContext();


       /* try {
           //  gifFromResource = new GifDrawable( getResources(), R.drawable.circle );
           //  gifFromResource.stop();


        } catch (IOException e) {
            e.printStackTrace();
        }*/

        donutProgress = view.findViewById(R.id.donut_progress);
        txt_cals = view.findViewById(R.id.txt_cals);

        Glide.with(context)
                .asBitmap()
                .apply(diskCacheStrategyOf(DiskCacheStrategy.DATA))
                .load(R.drawable.motion)
                .into(donutProgress);



        //  calcualteCalories = view.findViewById(R.id.calcualteCalories);
        btn_stop = view.findViewById(R.id.btn_stop);
        btn_start = view.findViewById(R.id.btn_start);

        tv_hours = view.findViewById(R.id.hours);
        tv_mints = view.findViewById(R.id.mints);
        tv_secs = view.findViewById(R.id.secs);

        df2.setRoundingMode(RoundingMode.UP);


        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds");
            startRun = savedInstanceState.getBoolean("startRun");
            hours = seconds / 3600;

        }

        Timer(view);

     /*   calcualteCalories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                res = getCaloriesBurn(programId , Integer.valueOf( currentuser.getWeight()) , seconds);
                updateDonat(65 , res);
            }
        });*/

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRun = true;
             //   gifFromResource.setLoopCount(0);
             //   gifFromResource.start();

                Glide.with(context)
                        .asGif()
                        .apply(diskCacheStrategyOf(DiskCacheStrategy.DATA))
                        .load(R.drawable.motion)
                        .into(donutProgress);

            }
        });

        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRun = false;
             //   gifFromResource.stop();
             //   gifFromResource.getError();

                Glide.with(context).onStop();

            }
        });


        return view;
    }

    private void Timer(View view) {

     //   final TextView timeView = view.findViewById(R.id.time_view);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                hours = seconds / 3600;
                int minutes = (seconds % 3600) / 60;
                int secs = seconds % 60;

                String time = String.format("%d:%02d:%02d", hours, minutes, secs);

           //     timeView.setText(time);

                String h = String.format("%d", hours);
                String m = String.format("%02d", minutes);
                String s = String.format("%02d", secs);


                tv_hours.setText(h);
                tv_mints.setText(m);
                tv_secs.setText(s);

                if (startRun) {
                    seconds++;
                }

                res = getCaloriesBurn(programId, Integer.valueOf(currentuser.getWeight()), seconds);

                updateDonat(65, res);

                handler.postDelayed(this, 1000);
            }
        });
    }


    // Calories = METS x Weight (Kgs) x Time (Hours)
    public double getCaloriesBurn(double melt, double weight, double time) {


        time = time / 3600;
        return (melt * weight * time);

    }

    public void onSaveInstanceState(Bundle saveInstanceState) {
        saveInstanceState.putInt("seconds", (int) seconds);
        saveInstanceState.putBoolean("startRun", startRun);
    }


    public void updateDonat(final int value, double res) {

      //  donutProgress.setDonut_progress(String.valueOf(value));
        txt_cals.setText(String.valueOf(String.format("%.2f", res)));

    }
}
