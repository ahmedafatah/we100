package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import eg.com.cat.fit100.Model.LoginResponse;
import eg.com.cat.fit100.Model.programsResponseModel;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.NavigationActivity;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Response;

import static eg.com.cat.fit100.View.MainActivity.currentuser;


public class DietFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    Spinner dropdown;
    Button button_home;
    String[] items, itemsIDs;
    String programId;
    private ProgressDialog progress;


    public DietFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_diet, container, false);


        //get the spinner from the xml.
        dropdown = view.findViewById(R.id.spinner1);

        button_home = view.findViewById(R.id.button_home);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog


        //create a list of items for the spinner.
        getDataFromServer();


        button_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                updateUserProgram();
            }
        });


        return view;

    }

    private void updateUserProgram() {

        progress.show();


        HashMap<String, String> data = new HashMap<>();

        data.put("user_id", currentuser.getId());
        data.put("program_id", programId);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        Response<LoginResponse> response = null;
        try {
            response = Webservice.getInstance().getApi().change_program(data).execute();

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!response.isSuccessful()) {
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getContext(), "Program Updated successfully", Toast.LENGTH_LONG).show();
            currentuser.setProgram_id(programId);
            gotoHome();
        }
        progress.dismiss();

    }

    private void gotoHome() {

        try {
            Intent k = new Intent(getActivity(), NavigationActivity.class);
            startActivity(k);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        ((TextView) v).setTextColor(Color.WHITE);

        Toast.makeText(getContext(), "You have selected " + items[+position], Toast.LENGTH_LONG).show();

        //    programId = String.valueOf(position + 1);
        programId = String.valueOf(itemsIDs[+position]);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void getDataFromServer() {

        progress.show();


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        Response<programsResponseModel> response = null;
        try {
            response = Webservice.getInstance().getApi().programs().execute();


        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!response.isSuccessful()) {
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), "Data Get Successfully", Toast.LENGTH_LONG).show();
            items = new String[response.body().getData().size()];
            for (int i = 0; i < response.body().getData().size(); ++i) {
                items[i] = response.body().getData().get(i).getTitle();

            }

            //create an adapter to describe how the items are displayed, adapters are used in several places in android.
            //There are multiple variations of this, but this is the basic variant.
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
            //set the spinners adapter to the previously created one.
            dropdown.setAdapter(adapter);

        }
        progress.dismiss();

        dropdown.setOnItemSelectedListener(this);
    }

}
