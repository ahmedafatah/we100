package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import eg.com.cat.fit100.Model.OffersResponseModel;
import eg.com.cat.fit100.Presenter.Adapters.EventsAdapter;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventsFragment extends Fragment {

    RecyclerView rcvOffers;
    OffersResponseModel offersResponseModel;
    EventsAdapter adapter;
    View rootView;

    ImageView home, offers, events, notification, bck , btn_newsfeed;
    private ProgressDialog progress;
    private Handler handler;

    TextView myempty;

    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_events, container, false);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog


        rcvOffers = rootView.findViewById(R.id.eventsDetails);
        myempty = rootView.findViewById(R.id.mytxtEmpty);
        btn_newsfeed = rootView.findViewById(R.id.btn_newsfeed);

        home = rootView.findViewById(R.id.btn_home);
        offers = rootView.findViewById(R.id.btn_offers);
        events = rootView.findViewById(R.id.btn_events);
        notification = rootView.findViewById(R.id.btn_notification);
        bck = rootView.findViewById(R.id.bck);


        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new OffersFragment())
                        .commit();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new NotificationFragment())
                        .commit();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });

        bck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });

        btn_newsfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new NewsFeedFragment())
                        .addToBackStack("btn_newsfeed")
                        .commit();
            }
        });


        getُEvents();

        return rootView;


    }



    private void getُEvents() {

        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {


                Webservice.getInstance().getApi().events().enqueue(new Callback<OffersResponseModel>() {
                    @Override
                    public void onResponse(Call<OffersResponseModel> call, Response<OffersResponseModel> response) {
                        if (response.isSuccessful()) {
                            progress.dismiss();
                            offersResponseModel = response.body();

                            if (offersResponseModel.getData().size() <= 0 )
                                myempty.setVisibility(View.VISIBLE);
                            else
                                myempty.setVisibility(View.GONE);


                            setOffersData();

                        } else {
                            Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            myempty.setVisibility(View.VISIBLE);

                        }
                    }

                    @Override
                    public void onFailure(Call<OffersResponseModel> call, Throwable t) {
                        Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                        progress.dismiss();

                    }
                });

            }
        }.start();


    }

    private void setOffersData() {
        adapter = new EventsAdapter(offersResponseModel, getActivity());
        rcvOffers.setAdapter(adapter);
        rcvOffers.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

}
