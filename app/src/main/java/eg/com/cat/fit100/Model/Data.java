package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("id")
    public int id;
    @SerializedName("country")
    public String country;
    @SerializedName("program_id")
    public String program_id;
    @SerializedName("name")
    public String name;
    @SerializedName("username")
    public String username;
    @SerializedName("email")
    public String email;
    @SerializedName("gender")
    public String gender;
    @SerializedName("height")
    public String height;
    @SerializedName("weight")
    public String weight;
    @SerializedName("bmi")
    public String bmi;
    @SerializedName("verified")
    public String verified;
    @SerializedName("active")
    public String active;
    @SerializedName("photos")
    public List<photos> photos;
}
