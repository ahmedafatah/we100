package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import eg.com.cat.fit100.Model.OffersResponseModel;
import eg.com.cat.fit100.Presenter.Adapters.OffersAdapter;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OffersFragment extends Fragment {

    RecyclerView rcvOffers;
    OffersResponseModel offersResponseModel;
    OffersAdapter adapter;
    View rootView;
    Handler handler;
    TextView myempty;

    ImageView home, offers, events, notification, bck;
    ProgressDialog progress;


    public OffersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_offers, container, false);


        home = rootView.findViewById(R.id.btn_home);
        offers = rootView.findViewById(R.id.btn_offers);
        events = rootView.findViewById(R.id.btn_events);
        notification = rootView.findViewById(R.id.btn_notification);
        bck = rootView.findViewById(R.id.bck);

        myempty = rootView.findViewById(R.id.mytxtEmpty);


        progress = new ProgressDialog(getActivity());

        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        rcvOffers = (RecyclerView) rootView.findViewById(R.id.offersDetails);
        getOffers();



        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });

        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new EventsFragment())
                        .commit();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new NotificationFragment())
                        .commit();
            }
        });

        bck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });


        return rootView;
    }

    public void initializeDesign() {

    }

    private void getOffers() {

        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {

                Webservice.getInstance().getApi().offers().enqueue(new Callback<OffersResponseModel>() {
                    @Override
                    public void onResponse(Call<OffersResponseModel> call, Response<OffersResponseModel> response) {
                        if (response.isSuccessful()) {

                            progress.dismiss();
                            offersResponseModel = response.body();

                            if (offersResponseModel.getData().size() <= 0 )
                                myempty.setVisibility(View.VISIBLE);
                            else
                                myempty.setVisibility(View.GONE);

                            setOffersData();

                        } else {
                            Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            myempty.setVisibility(View.VISIBLE);

                        }
                    }

                    @Override
                    public void onFailure(Call<OffersResponseModel> call, Throwable t) {
                        Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                        progress.dismiss();

                    }
                });


            }
        }.start();
    }

    private void setOffersData() {
        adapter = new OffersAdapter(offersResponseModel, getActivity());
        rcvOffers.setAdapter(adapter);
        rcvOffers.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rcvOffers.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


}
