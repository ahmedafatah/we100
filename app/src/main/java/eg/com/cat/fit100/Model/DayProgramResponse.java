package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DayProgramResponse {

    @SerializedName("DATA")
    public List<Data> DATA;
    @SerializedName("state")
    public String state;

    public List<Data> getDATA() {
        return DATA;
    }

    public void setDATA(List<Data> DATA) {
        this.DATA = DATA;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public class Data {
        @SerializedName("id")
        public int id;
        @SerializedName("title")
        public String title;
        @SerializedName("brief")
        public String brief;
        @SerializedName("photos")
        public List<photos> photos;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBrief() {
            return brief;
        }

        public void setBrief(String brief) {
            this.brief = brief;
        }

        public List<photos> getPhotos() {
            return photos;
        }

        public void setPhotos(List<photos> photos) {
            this.photos = photos;
        }
    }


}
