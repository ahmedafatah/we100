package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.juanpabloprado.countrypicker.CountryPicker;
import com.juanpabloprado.countrypicker.CountryPickerListener;

import org.json.JSONObject;

import java.util.HashMap;

import eg.com.cat.fit100.Model.RegisterResponseModel;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment {

    String gender = "1";
    String country;
    EditText username, email, password, confirm_password, weight, height , fullname; //bmi;
    TextView country_name;
    LinearLayout country_picker_search;
    Button register;
    RadioGroup genderRadioGroup;
    CountryPicker picker;
    ProgressDialog progress;
    private Handler handler;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);


        username = (EditText) view.findViewById(R.id.input_username);
        fullname = view.findViewById(R.id.input_fullname);
        email = (EditText) view.findViewById(R.id.input_email);
        password = (EditText) view.findViewById(R.id.input_password);
        confirm_password = (EditText) view.findViewById(R.id.input_confirmPassword);
        weight = (EditText) view.findViewById(R.id.input_weight);
        height = (EditText) view.findViewById(R.id.input_height);
        //   bmi = (EditText) view.findViewById(R.id.input_bmi);
        register = (Button) view.findViewById(R.id.button_register);
        country_picker_search = view.findViewById(R.id.country_picker_search);
        country_name = view.findViewById(R.id.country_name);


        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        genderRadioGroup = (RadioGroup) view.findViewById(R.id.genderRadioGroup);

        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.maleRadioButton: {
                        gender = "1";
                        break;
                    }
                    case R.id.femaleRadioButton: {
                        gender = "0";
                        break;
                    }
                }
            }
        });


        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                register();
            }
        });


        country_picker_search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setCountry();
            }
        });


        return view;
    }


    private void register() {

        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {

                HashMap<String, String> user = new HashMap<>();

                user.put("name", fullname.getText().toString().trim());
                user.put("username", username.getText().toString().trim());
                user.put("email", email.getText().toString().trim());
                user.put("password", password.getText().toString().trim());
                user.put("password_confirmation", confirm_password.getText().toString().trim());
                user.put("weight", weight.getText().toString().trim());
                user.put("height", height.getText().toString().trim());
                //     user.put("bmi", bmi.getText().toString().trim());
                user.put("gender", gender);
                user.put("country", country);


                Webservice.getInstance().getApi().Register(user).enqueue(new Callback<RegisterResponseModel>() {
                    @Override
                    public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                        if (!response.isSuccessful()) {
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
                            } catch (Exception e) {
                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                            }

                            progress.dismiss();

                        } else {
                            progress.dismiss();

                            Toast.makeText(getActivity(), "Registration Successful, moving you to homepage now.", Toast.LENGTH_LONG).show();
                            goToLogin();

                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                        Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                        progress.dismiss();

                    }
                });



            }
        }.start();
    }

    private void goToLogin() {

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new LoginFragment())
                .addToBackStack("Login")
                .commit();

    }


    public void setCountry() {
        picker = CountryPicker.getInstance("Select Country", new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code) {
                //   Toast.makeText(getActivity(), "Name: " + name, Toast.LENGTH_SHORT).show();
                country = name;
                country_name.setText(country);
                DialogFragment dialogFragment =
                        (DialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag("CountryPicker");
                dialogFragment.dismiss();
            }
        });
        picker.show(getActivity().getSupportFragmentManager(), "CountryPicker");
    }
}
