package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddPostResponseModel {

    @SerializedName("DATA")
    public Data DATA;
    @SerializedName("state")
    public String state;

    public class Data {
        @SerializedName("id")
        public String id;
        @SerializedName("user_id")
        public String user_id;
        @SerializedName("body")
        public String body;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("photos")
        public List<photos> photos;

    }

}