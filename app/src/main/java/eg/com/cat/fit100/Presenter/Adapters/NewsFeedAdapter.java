package eg.com.cat.fit100.Presenter.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import eg.com.cat.fit100.Model.AddCommentResponseModel;
import eg.com.cat.fit100.Model.PostsResponseModel;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Response;

import static eg.com.cat.fit100.View.MainActivity.currentuser;
import static eg.com.cat.fit100.View.MainActivity.getContext;
import static eg.com.cat.fit100.WebServices.Services.MAIN_PICTURES_URL;

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.NewsFeedHolder> {

    public PostsResponseModel postsResponseModel;
    private Context context;
    private View inflatedView;
    private PopupWindow popWindow;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private Handler handler;
    private static Activity parent;
    String toastMessage = "Please Wait";
    ArrayList<String> comments = new ArrayList<String>();
    ProgressDialog progress;

    public NewsFeedAdapter(PostsResponseModel postsResponseModel, Context context) {
        this.postsResponseModel = postsResponseModel;
        this.context = context;
    }


    @NonNull
    @Override
    public NewsFeedAdapter.NewsFeedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NewsFeedAdapter.NewsFeedHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_posts_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final NewsFeedAdapter.NewsFeedHolder holder, final int position) {

        if (postsResponseModel.getDATA().get(position).getPhotos() != null &&
                postsResponseModel.getDATA().get(position).getPhotos().size() != 0
        )
            if (postsResponseModel.getDATA().get(position).getPhotos().get(0).getPath() != null) {

                holder.postImage.setVisibility(View.VISIBLE);

                Picasso.get().load(MAIN_PICTURES_URL +
                        postsResponseModel.getDATA().get(position).getPhotos().get(0).getPath())
                        .fit()

                        //       .centerInside()
                        .into(holder.postImage, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError(Exception e) {
                                holder.postImage.setVisibility(View.GONE);
                            }
                        });

            } else {
                holder.postImage.setVisibility(View.GONE);

            }
        else {
            holder.postImage.setVisibility(View.GONE);

        }

        if (postsResponseModel.getDATA().get(position).getProfile_picture() != null &&
                !postsResponseModel.getDATA().get(position).getProfile_picture().equals("")
        ) {

            Picasso.get().load(MAIN_PICTURES_URL +
                    postsResponseModel.getDATA().get(position).getProfile_picture())
                    .fit()

                    //       .centerInside()
                    .into(holder.postprofilepicture);
        }

        holder.tvDate.setText(postsResponseModel.getDATA().get(position).getCreated_at());

        holder.likesCount.setText(postsResponseModel.getDATA().get(position).getCount_likes());
        holder.commentsCount.setText(postsResponseModel.getDATA().get(position).getCount_comment() );


        if (postsResponseModel.getDATA().get(position).getCheck_liked().equals("1")) {
            holder.likepost.setImageResource(R.drawable.likeactiv);
        }

        holder.username.setText(postsResponseModel.getDATA().get(position).getUser());

        holder.post.setText(postsResponseModel.getDATA().get(position).getBody());

        holder.likepost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likepost(postsResponseModel.getDATA().get(position).getId(), currentuser.getId() , holder , position);
            }
        });

        holder.commentpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowPopup(v, postsResponseModel.getDATA().get(position).getId());
            }
        });

        holder.sharepost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shareBody = postsResponseModel.getDATA().get(position).getBody();
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "info from FIT180");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share using : "));

            }
        });


        holder.likesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowPopup(v, postsResponseModel.getDATA().get(position).getId());
            }
        });

    }

    private String likesCount(int position, Boolean like) {
        int  current = Integer.valueOf(postsResponseModel.getDATA().get(position).getCount_likes());
        if (like) {
            ++current;
            postsResponseModel.getDATA().get(position).setCount_likes(String.valueOf(current));
            postsResponseModel.getDATA().get(position).setCheck_liked("1");
        }
        else
        {
            --current;
            postsResponseModel.getDATA().get(position).setCount_likes(String.valueOf(current));
            postsResponseModel.getDATA().get(position).setCheck_liked("0");
        }
        return String.valueOf(current);
    }

    @Override
    public int getItemCount() {
        return postsResponseModel.getDATA().size();
    }

    void setSimpleList(ListView listView, String id) {

         ArrayList<String> contactsList = getComments(id);

        listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1
                , android.R.id.text1, contactsList));

     }

    // call this method when required to show popup
    public void onShowPopup(View v, final String id) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        inflatedView = layoutInflater.inflate(R.layout.comment_popup_layout, null, false);
        // find the ListView in the popup layout
        ListView listView = (ListView) inflatedView.findViewById(R.id.commentsListView);
        ImageView sendComment = (ImageView) inflatedView.findViewById(R.id.sendcomment);
        final EditText writeComment = (EditText) inflatedView.findViewById(R.id.writeComment);
        ProgressBar progress_bar;
        progress_bar = inflatedView.findViewById(R.id.progress_bar);

        progress_bar.setVisibility(View.VISIBLE);
        // fill the data to the list items
        setSimpleList(listView, id);

        progress_bar.setVisibility(View.GONE);


        // get device size
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;





        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, width, height - 50, true);
        // set a background drawable with rounders corners
        popWindow.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.dialog_bg_white));

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(v, Gravity.BOTTOM, 0, 100);

        sendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment(id, currentuser.getId(), writeComment.getText().toString());
            }
        });


    }

    private void sendComment(final String id, final String userid, final String s) {

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };

        new Thread() {
            public void run() {

                HashMap<String, String> data = new HashMap<>();

                data.put("user_id", userid);
                data.put("body", s);

                Response<AddCommentResponseModel> response = null;
                try {
                    response = Webservice.getInstance().getApi().addComment(id, data).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                         toastMessage = "Error";

                    } catch (Exception e) {
                        toastMessage = "Error";
                    }
                } else {
                    AddCommentResponseModel addCommentResponseModel = response.body();
                     toastMessage = "Comment posted Successfully";
                }


            }

        }.start();

        Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();

    }

    private void likepost(final String id, final String currentuserId, final NewsFeedHolder holder , final int position) {

                    final Boolean like;
                    if (postsResponseModel.getDATA().get(position).getCheck_liked().equals("1")) {
                        like = false;
                    }

                    else  like = true;

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }
        };

        new Thread() {
            public void run() {


                HashMap<String, String> data = new HashMap<>();
                    data.put("user_id", currentuserId);
                    Response<AddCommentResponseModel> response = null;
                    try {
                        response = Webservice.getInstance().getApi().addLike(id, data).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            toastMessage = "Error Please Try again later";
                        } catch (Exception e) {
                        }
                    } else {
                        AddCommentResponseModel addCommentResponseModel = response.body();
                        updateUIwithLike(holder , position , like);

                        if (like)
                        toastMessage = "Liked Successfully";
                        else
                            toastMessage = "Like Removed Successfully";


                    }

            }

        }.start();


    }

    private void updateUIwithLike(final NewsFeedHolder holder, final int position, final Boolean like) {


        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (like)
                    holder.likepost.setImageResource(R.drawable.likeactiv);
                else
                    holder.likepost.setImageResource(R.drawable.like);

                holder.likesCount.setText(likesCount(position , like) );
                holder.commentsCount.setText(  postsResponseModel.getDATA().get(position).getCount_comment() );
                Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();

            }
        });


    }


    private ArrayList<String> getComments(final String id) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        comments = new ArrayList<String>();

        final Response<AddCommentResponseModel> response;
        try {
            response = Webservice.getInstance().getApi().postComments(id).execute();
            if (!response.isSuccessful()) {
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getString("data"), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                AddCommentResponseModel addCommentResponseModel = response.body();

                for (int i = 0; i < addCommentResponseModel.getDATA().getData().size(); ++i)
                    comments.add(addCommentResponseModel.getDATA().getData().get(i).getBody());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return comments;
    }

    public class NewsFeedHolder extends RecyclerView.ViewHolder {

        TextView post, username, tvDate, likesCount, commentsCount;
        ImageView postprofilepicture, postImage;
        ImageView likepost;
        ImageView commentpost;
        ImageView sharepost;

        public NewsFeedHolder(View itemView) {
            super(itemView);

            post = itemView.findViewById(R.id.tvContent);
            postprofilepicture = itemView.findViewById(R.id.imgProfile);
            likepost = itemView.findViewById(R.id.like);
            commentpost = itemView.findViewById(R.id.comment);
            sharepost = itemView.findViewById(R.id.share);
            postImage = itemView.findViewById(R.id.imgPost);
            username = itemView.findViewById(R.id.tvUserName);
            tvDate = itemView.findViewById(R.id.tvDate);
            likesCount = itemView.findViewById(R.id.tvnNoLikesComments);
            commentsCount = itemView.findViewById(R.id.tvNoComments);
        }
    }

}
