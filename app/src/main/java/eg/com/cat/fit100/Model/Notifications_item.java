package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notifications_item {

    @SerializedName("data")
    public List<data> data;
    @SerializedName("state")
    public String state;


    public List<Notifications_item.data> getData() {
        return data;
    }

    public String getState() {
        return state;
    }

    public class data {
        @SerializedName("id")
        public int id;
        @SerializedName("title")
        public String title;
        @SerializedName("notifiable")
        public notifiable notifiable;

        @SerializedName("updated_at")
        public String updated_at;

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public Notifications_item.data.notifiable getNotifiable() {
            return notifiable;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public class notifiable {
            @SerializedName("id")
            public int id;
            @SerializedName("title")
            public String title;
            @SerializedName("body")
            public String body;

            public int getId() {
                return id;
            }

            public String getTitle() {
                return title;
            }

            public String getBody() {
                return body;
            }
        }

    }


}
