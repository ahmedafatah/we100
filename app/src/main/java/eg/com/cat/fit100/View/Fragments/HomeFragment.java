package eg.com.cat.fit100.View.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import eg.com.cat.fit100.Model.ImageModel;
import eg.com.cat.fit100.Model.PostsResponseModel;
import eg.com.cat.fit100.Presenter.Adapters.NewsFeedAdapter;
import eg.com.cat.fit100.Presenter.FileUtils;
import eg.com.cat.fit100.Presenter.SwipeController;
import eg.com.cat.fit100.Presenter.SwipeControllerActions;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.ActivityActivity;
import eg.com.cat.fit100.View.ExerciseActivity;
import eg.com.cat.fit100.View.FoodActivity;
import eg.com.cat.fit100.WebServices.Webservice;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static eg.com.cat.fit100.View.MainActivity.currentuser;


public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    private static ViewPager healthTipsPager, postsPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static Handler handler;
    CirclePageIndicator indicator, postsIndicator;
    ImageView activity, exercise, diet, home, offers, events, notification, tobeUploadpic, btn_edit_profile, myprofilepicture, btn_newsfeed;
    EditText addpost_edittext;
    Button post_picture;
    TextView myempty;

    String strBase64 = null;
    TextView sendpost;
    ProgressDialog progress;
    PostsResponseModel postsResponseModel;
    RecyclerView rcvOffers;
    NewsFeedAdapter adapter;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private Uri uri;
    private ArrayList<ImageModel> imageModelArrayList;
    private int[] myImageList = new int[]{R.drawable.s10_02, R.drawable.s10_02,
            R.drawable.s10_02, R.drawable.s10_02};


    SwipeController swipeController = null;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        activity = view.findViewById(R.id.actvity);
        exercise = view.findViewById(R.id.excrsie);
        diet = view.findViewById(R.id.diet);

        home = view.findViewById(R.id.btn_home);
        offers = view.findViewById(R.id.btn_offers);
        events = view.findViewById(R.id.btn_events);
        notification = view.findViewById(R.id.btn_notification);
        btn_edit_profile = view.findViewById(R.id.btn_edit_profile);
        sendpost = view.findViewById(R.id.addpost_edittext);
        btn_newsfeed = view.findViewById(R.id.btn_newsfeed);
        rcvOffers = view.findViewById(R.id.postsDetails);
        myempty = view.findViewById(R.id.mytxtEmpty);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

/*

        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();


        final float density = getResources().getDisplayMetrics().density;


        NUM_PAGES = imageModelArrayList.size();


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                //     healthTipsPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*/


        activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent k = new Intent(getContext(), ActivityActivity.class);
                    startActivity(k);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        exercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent k = new Intent(getContext(), ExerciseActivity.class);
                    startActivity(k);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        diet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent k = new Intent(getContext(), FoodActivity.class);
                    startActivity(k);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new OffersFragment())
                        .addToBackStack("OffersFragment")
                        .commit();
            }
        });

        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new EventsFragment())
                        .addToBackStack("EventsFragment")
                        .commit();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new NotificationFragment())
                        .addToBackStack("NotificationFragment")
                        .commit();

            }
        });

        btn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new EditProfileFragment())
                        .addToBackStack("EditProfileFragment")
                        .commit();
            }
        });


        sendpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post();
            }
        });

        btn_newsfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new NewsFeedFragment())
                        .addToBackStack("btn_newsfeed")
                        .commit();

            }
        });

        getPosts();

        changePlayerID();


        return view;
    }

    private void getPosts() {


        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {

                Webservice.getInstance().getApi().getposts(currentuser.getId()).enqueue(new Callback<PostsResponseModel>() {
                    @Override
                    public void onResponse(Call<PostsResponseModel> call, Response<PostsResponseModel> response) {
                        if (response.isSuccessful()) {

                            progress.dismiss();
                            postsResponseModel = response.body();

                            if (postsResponseModel.getDATA().size() <= 0)
                                myempty.setVisibility(View.VISIBLE);
                            else
                                myempty.setVisibility(View.GONE);

                            setOffersData();

                        } else {
                            Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            myempty.setVisibility(View.VISIBLE);

                        }
                    }

                    @Override
                    public void onFailure(Call<PostsResponseModel> call, Throwable t) {
                        Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                        progress.dismiss();

                    }
                });
            }
        }.start();

    }

    private void setOffersData() {
        adapter = new NewsFeedAdapter(postsResponseModel, getActivity());
        rcvOffers.setAdapter(adapter);
        rcvOffers.setLayoutManager(new LinearLayoutManager(getActivity()));

        swipeController = new SwipeController(postsResponseModel , new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {

                removepost(currentuser.getId() , postsResponseModel.getDATA().get(position).getId());

                adapter.postsResponseModel.getDATA().remove(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, adapter.getItemCount());


            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(rcvOffers);


        rcvOffers.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });


        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));


        rcvOffers.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.HORIZONTAL));

        rcvOffers.setHasFixedSize(true);
    }



    private void post() {

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new newPostFragment())
                .commit();

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            uri = data.getData();
            uploadFile(uri);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void uploadFile(Uri fileUri) {

        File file = FileUtils.getFile(getContext(), fileUri);
        final Bitmap selectedImage = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 30, stream);
        byte[] byteArray = stream.toByteArray();
        strBase64 = Base64.encodeToString(byteArray, 0);
        tobeUploadpic.setImageBitmap(selectedImage);
    }




    public void changePlayerID() {


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {

                final HashMap<String, String> data = new HashMap<>();


                OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
                status.getPermissionStatus().getEnabled();

                String playerId = status.getSubscriptionStatus().getUserId();
                //   Log.d("PlayerId", playerId);


                data.put("user_id", currentuser.getId());
                data.put("player_id", playerId);


                Response<Object> response = null;
                try {
                    response = Webservice.getInstance().getApi().update_player_id(data).execute();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d(TAG, "changePlayerID: " + response.body().toString());
                    } catch (Exception e) {
                        Log.d(TAG, "changePlayerID: " + e.toString());
                    }
                } else {
                    Log.d(TAG, "changePlayerID: " + response.body().toString());
                }

            }
        }.start();
    }

    private void removepost(final String id, final String postId) {

        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };

        new Thread() {
            public void run() {

                HashMap<String, String> data = new HashMap<>();

                data.put("user_id", id);

                Response<Object> response = null;
                try {
                    response = Webservice.getInstance().getApi().delete_post(postId , data).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (Exception e) {
                    }

                    progress.dismiss();

                } else {
                    progress.dismiss();

                }
            }


        }.start();

    }



}
