package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class programsResponseModel {

    @SerializedName("data")
    public List<ProgramData> data;
    @SerializedName("state")
    public String state;

    public List<ProgramData> getData() {
        return data;
    }

    public void setData(List<ProgramData> data) {
        this.data = data;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public class ProgramData {
        @SerializedName("id")
        public String id;
        @SerializedName("title")
        public String title;
        @SerializedName("brief")
        public String brief;
        @SerializedName("active")
        public String active;
        @SerializedName("photos")
        public List<photos> photos;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBrief() {
            return brief;
        }

        public void setBrief(String brief) {
            this.brief = brief;
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }

        public List<photos> getPhotos() {
            return photos;
        }

        public void setPhotos(List<photos> photos) {
            this.photos = photos;
        }
    }


}
