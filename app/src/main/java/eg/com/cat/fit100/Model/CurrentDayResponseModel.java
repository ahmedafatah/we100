package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrentDayResponseModel {

    @SerializedName("data")
    public List<Data> data;
    @SerializedName("state")
    public String state;


    public List<Data> getData() {
        return data;
    }

    public String getState() {
        return state;
    }

    public class Data {
        @SerializedName("id")
        public int id;
        @SerializedName("title")
        public String title;
        @SerializedName("brief")
        public String brief;
        @SerializedName("photos")
        public List<photos> photos;


        @SerializedName("meals")
        public List<meals> meals;

        @SerializedName("exercises")
        public List<exercises> exercises;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBrief() {
            return brief;
        }

        public void setBrief(String brief) {
            this.brief = brief;
        }

        public List<eg.com.cat.fit100.Model.photos> getPhotos() {
            return photos;
        }

        public void setPhotos(List<eg.com.cat.fit100.Model.photos> photos) {
            this.photos = photos;
        }

        public List<CurrentDayResponseModel.meals> getMeals() {
            return meals;
        }

        public void setMeals(List<CurrentDayResponseModel.meals> meals) {
            this.meals = meals;
        }

        public List<CurrentDayResponseModel.exercises> getExercises() {
            return exercises;
        }

        public void setExercises(List<CurrentDayResponseModel.exercises> exercises) {
            this.exercises = exercises;
        }
    }

    public class meals {

        @SerializedName("id")
        public int id;
        @SerializedName("title")
        public String title;
        @SerializedName("schedule_id")
        public String schedule_id;
        @SerializedName("body")
        public String body;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSchedule_id() {
            return schedule_id;
        }

        public void setSchedule_id(String schedule_id) {
            this.schedule_id = schedule_id;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }

    public class exercises {
        @SerializedName("id")
        public int id;
        @SerializedName("title")
        public String title;
        @SerializedName("schedule_id")
        public String schedule_id;
        @SerializedName("body")
        public String body;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSchedule_id() {
            return schedule_id;
        }

        public void setSchedule_id(String schedule_id) {
            this.schedule_id = schedule_id;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }
}
