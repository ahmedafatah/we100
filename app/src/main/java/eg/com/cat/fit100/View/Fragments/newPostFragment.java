package eg.com.cat.fit100.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import eg.com.cat.fit100.Model.AddPostResponseModel;
import eg.com.cat.fit100.Model.ImageModel;
import eg.com.cat.fit100.Presenter.FileUtils;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;
import retrofit2.Retrofit;

import static eg.com.cat.fit100.View.MainActivity.currentuser;


public class newPostFragment extends Fragment {

    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    private static int RESULT_LOAD_IMAGE = 1;
    private static ViewPager healthTipsPager, postsPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    TextView share;
    ImageView img, bck;
    EditText title, content;
    Button addPhoto;
    String url, userId, userName, titletxt, contenttxt;
    SharedPreferences shared;
    String encoded = "";
    int newsId = 0;
    String strBase64 = null;
    CirclePageIndicator indicator, postsIndicator;
    ImageView activity, exercise, diet, home, offers, events, notification, tobeUploadpic, btn_edit_profile, myprofilepicture, btn_newsfeed;
    EditText addpost_edittext;
    Button post_picture;
    Button sendpost;
    ProgressDialog progress;
    Retrofit retrofit;
    private Uri uri;
    private ArrayList<ImageModel> imageModelArrayList;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private int[] myImageList = new int[]{R.drawable.s10_02, R.drawable.s10_02,
            R.drawable.s10_02, R.drawable.s10_02};
    private Handler handler;

    String toastMessage;


    public newPostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_post, container, false);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog


        share = view.findViewById(R.id.tvShare);
        img = view.findViewById(R.id.img);
        bck = view.findViewById(R.id.bck);
        addPhoto = view.findViewById(R.id.btnAddPhoto);
        addpost_edittext = view.findViewById(R.id.etContent);


        bck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share.startAnimation(buttonClick);
                post();

            }
        });

        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                uploadPicture();
            }
        });


        return view;
    }

    private void post() {

        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {

                HashMap<String, String> data = new HashMap<>();


                if (addpost_edittext.getText().toString().trim().isEmpty())
                         toastMessage = "Please enter your post correctly " ;

                else {
                    data.put("user_id", currentuser.getId());
                    data.put("body", addpost_edittext.getText().toString().trim());

                    if (strBase64 != null)
                        data.put("image", strBase64);

                    Response<AddPostResponseModel> response = null;
                    try {
                        response = Webservice.getInstance().getApi().AddPosts(data).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            toastMessage = "Error" ;
                        } catch (Exception e) {
                            toastMessage = "Error" ;
                        }

                        progress.dismiss();

                    } else {
                        AddPostResponseModel addPostResponseModel = response.body();
                        progress.dismiss();
                        addpost_edittext.setText("");
                  //      Toast.makeText(getActivity(), "Posted Successfully", Toast.LENGTH_LONG).show();
                        toastMessage = "Posted Successfully" ;

                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .add(R.id.container, new HomeFragment())
                                .commit();

                    }

                }
            }


        }.start();

        Toast.makeText(getContext(), toastMessage, Toast.LENGTH_LONG).show();
    }

    private void uploadPicture() {

        if (EasyPermissions.hasPermissions(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
            openGalleryIntent.setType("image/*");
            startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(getActivity(), "Gallery Permission",
                    READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            uri = data.getData();
            uploadFile(uri);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void uploadFile(Uri fileUri) {

        File file = FileUtils.getFile(getContext(), fileUri);
        final Bitmap selectedImage = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 30, stream);
        byte[] byteArray = stream.toByteArray();
        strBase64 = Base64.encodeToString(byteArray, 0);
        img.setImageBitmap(selectedImage);
    }


}
