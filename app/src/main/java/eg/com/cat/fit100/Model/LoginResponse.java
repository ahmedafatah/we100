package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("data")
    public Data data;
    @SerializedName("state")
    public String state;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
