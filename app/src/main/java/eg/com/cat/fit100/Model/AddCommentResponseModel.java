package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddCommentResponseModel {

    @SerializedName("DATA")
    public Data DATA;
    @SerializedName("state")
    public String state;

    public Data getDATA() {
        return DATA;
    }

    public void setDATA(Data DATA) {
        this.DATA = DATA;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public class Data {

        @SerializedName("data")
        public List<data> data;

        public List<Data.data> getData() {
            return data;
        }

        public void setData(List<Data.data> data) {
            this.data = data;
        }

        public class data {
            @SerializedName("id")
            public String id;
            @SerializedName("post_id")
            public String post_id;
            @SerializedName("user_id")
            public String user_id;
            @SerializedName("body")
            public String body;
            @SerializedName("updated_at")
            public String updated_at;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getBody() {
                return body;
            }

            public void setBody(String body) {
                this.body = body;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
