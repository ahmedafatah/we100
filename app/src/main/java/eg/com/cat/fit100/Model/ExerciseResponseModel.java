package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExerciseResponseModel {


    @SerializedName("DATA")
    public List<Data> DATA;
    @SerializedName("state")
    public String state;

    public List<Data> getDATA() {
        return DATA;
    }

    public void setDATA(List<Data> DATA) {
        this.DATA = DATA;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public class Data {
        @SerializedName("id")
        public String id;
        @SerializedName("schedule_id")
        public String schedule_id;
        @SerializedName("title")
        public String title;
        @SerializedName("body")
        public String body;
        @SerializedName("photos")
        public List<photos> photos;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSchedule_id() {
            return schedule_id;
        }

        public void setSchedule_id(String schedule_id) {
            this.schedule_id = schedule_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public List<photos> getPhotos() {
            return photos;
        }

        public void setPhotos(List<photos> photos) {
            this.photos = photos;
        }
    }

}
