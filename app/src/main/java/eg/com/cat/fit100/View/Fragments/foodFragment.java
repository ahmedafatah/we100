package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import eg.com.cat.fit100.Model.ConfirmationResponse;
import eg.com.cat.fit100.Model.CurrentDayResponseModel;
import eg.com.cat.fit100.Presenter.Adapters.foodAdapter;
import eg.com.cat.fit100.Presenter.RecyclerViewMargin;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.MainActivity;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static eg.com.cat.fit100.View.MainActivity.currentuser;

public class foodFragment extends Fragment {

    static CurrentDayResponseModel currentDayResponseModel;
    static ConfirmationResponse confirmationResponse;
    RecyclerView rcvOffers;
    foodAdapter adapter;
    View rootView;
    ProgressDialog progress;
    private Handler handler;
    TextView myempty;



    public foodFragment() {
        // Required empty public constructor
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 120);
        return noOfColumns;
    }

    public static void updateUserProgram() {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        HashMap<String, String> userid = new HashMap<>();

        userid.put("user_id", currentuser.getId());
        userid.put("schedule_id", currentDayResponseModel.getData().get(0).getMeals().get(0).getSchedule_id());


        Webservice.getInstance().getApi().confirmMeal(userid).enqueue(new Callback<ConfirmationResponse>() {
            @Override
            public void onResponse(Call<ConfirmationResponse> call, Response<ConfirmationResponse> response) {
                if (response.isSuccessful()) {
                    confirmationResponse = response.body();
                    Toast.makeText(MainActivity.getContext(), "Day Completed,", Toast.LENGTH_LONG).show();


                } else {
                    Toast.makeText(MainActivity.getContext(), "failure", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ConfirmationResponse> call, Throwable t) {
                Toast.makeText(MainActivity.getContext(), "failure , check your connection", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_food, container, false);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        rcvOffers = (RecyclerView) rootView.findViewById(R.id.mainfoodDetails);
        myempty = rootView.findViewById(R.id.mytxtEmpty);

        getُMeals();

        return rootView;
    }


    public void getُMeals() {


        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {


                HashMap<String, String> userid = new HashMap<>();

                userid.put("user_id", currentuser.getId());


                Webservice.getInstance().getApi().getMealsbyDay(userid).enqueue(new Callback<CurrentDayResponseModel>() {
                    @Override
                    public void onResponse(Call<CurrentDayResponseModel> call, Response<CurrentDayResponseModel> response) {
                        if (response.isSuccessful()) {
                            progress.dismiss();

                            currentDayResponseModel = response.body();

                            if (currentDayResponseModel.getData().size() <= 0 )
                                myempty.setVisibility(View.VISIBLE);
                            else
                                myempty.setVisibility(View.GONE);

                            setOffersData();

                        } else {
                            progress.dismiss();

                            Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CurrentDayResponseModel> call, Throwable t) {
                        progress.dismiss();

                        Toast.makeText(getActivity(), "the Last program is finished, please choose a new one", Toast.LENGTH_LONG).show();
                    }
                });

            }
        }.start();

    }

    private void setOffersData() {
        adapter = new foodAdapter(currentDayResponseModel, getActivity());
        rcvOffers.setAdapter(adapter);
        //    rcvOffers.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.HORIZONTAL));


        //  LinearLayoutManager horizontalLayoutManagaer
        //          = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        //   rcvOffers.setLayoutManager(horizontalLayoutManagaer);

        int mNoOfColumns = calculateNoOfColumns(getContext());
        rcvOffers.setLayoutManager(new GridLayoutManager(getContext(), 2));

        // rcvOffers.setLayoutManager(new LinearLayoutManager(getActivity()));

        RecyclerViewMargin decoration = new RecyclerViewMargin(15, 2);
        rcvOffers.addItemDecoration(decoration);

    }
}
