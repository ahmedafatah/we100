package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import eg.com.cat.fit100.Model.ConfirmationResponse;
import eg.com.cat.fit100.Model.CurrentDayResponseModel;
import eg.com.cat.fit100.Presenter.Adapters.ExerciseAdapter;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.MainActivity;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static eg.com.cat.fit100.View.MainActivity.currentuser;


public class ExerciseDetailsFragment extends Fragment {

    static CurrentDayResponseModel currentDayResponseModel;
    static ProgressDialog progress;
    static ConfirmationResponse confirmationResponse;
    RecyclerView rcvOffers;
    ExerciseAdapter adapter;
    View rootView;
    String dayid;
    TextView myempty;
    private Handler handler;


    public ExerciseDetailsFragment() {
        // Required empty public constructor
    }

    public static void updateUserProgram() {

        progress.show();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        HashMap<String, String> userid = new HashMap<>();

        userid.put("user_id", currentuser.getId());
        userid.put("schedule_id", currentDayResponseModel.getData().get(0).getExercises().get(0).getSchedule_id());


        Webservice.getInstance().getApi().confirmExercise(userid).enqueue(new Callback<ConfirmationResponse>() {
            @Override
            public void onResponse(Call<ConfirmationResponse> call, Response<ConfirmationResponse> response) {
                if (response.isSuccessful()) {
                    confirmationResponse = response.body();
                    Toast.makeText(MainActivity.getContext(), "Day Completed,", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(MainActivity.getContext(), "failure", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ConfirmationResponse> call, Throwable t) {
                Toast.makeText(MainActivity.getContext(), "the Last program is finished, please choose a new one", Toast.LENGTH_LONG).show();
            }
        });


        progress.dismiss();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_exercise_details, container, false);
        //   dayid = getArguments().getString("DAYID");


        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        myempty = rootView.findViewById(R.id.mytxtEmpty);

        rcvOffers = (RecyclerView) rootView.findViewById(R.id.exerciseDetails);

        getExercise();


        return rootView;
    }

    private void getExercise() {


        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {


                HashMap<String, String> userid = new HashMap<>();

        userid.put("user_id", currentuser.getId());


        Webservice.getInstance().getApi().getExercisebyDay(userid).enqueue(new Callback<CurrentDayResponseModel>() {
            @Override
            public void onResponse(Call<CurrentDayResponseModel> call, Response<CurrentDayResponseModel> response) {
                if (response.isSuccessful()) {
                    progress.dismiss();

                    currentDayResponseModel = response.body();

                    if (currentDayResponseModel.getData().size() <= 0 )
                        myempty.setVisibility(View.VISIBLE);
                    else
                        myempty.setVisibility(View.GONE);

                    setOffersData();

                } else {
                    Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                    progress.dismiss();

                }
            }

            @Override
            public void onFailure(Call<CurrentDayResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                progress.dismiss();

            }
        });


            }
        }.start();

    }

    private void setOffersData() {
        adapter = new ExerciseAdapter(currentDayResponseModel, getActivity());
        rcvOffers.setAdapter(adapter);
        rcvOffers.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


}
