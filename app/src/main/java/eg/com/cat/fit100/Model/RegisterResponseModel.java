package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

public class RegisterResponseModel {

    @SerializedName("data")
    public String data;
    @SerializedName("state")
    public String state;

    public String getData() {
        return data;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
