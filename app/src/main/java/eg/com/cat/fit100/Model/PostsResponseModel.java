package eg.com.cat.fit100.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostsResponseModel {



    @SerializedName("data")
    public List<Data> DATA;
    @SerializedName("state")
    public String state;

    public List<Data> getDATA() {
        return DATA;
    }

    public void setDATA(List<Data> DATA) {
        this.DATA = DATA;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public class Data {
        @SerializedName("id")
        public String id;
        @SerializedName("user_id")
        public String user_id;
        @SerializedName("body")
        public String body;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("photos")
        public List<photos> photos;
        @SerializedName("user")
        public String user;
        @SerializedName("count_comment")
        public String count_comment;
        @SerializedName("count_likes")
        public String count_likes;
        @SerializedName("check_liked")
        public String check_liked;
        @SerializedName("profile_picture")
        public String profile_picture;

        public String getCount_comment() {
            return count_comment;
        }

        public void setCount_comment(String count_comment) {
            this.count_comment = count_comment;
        }

        public String getCount_likes() {
            return count_likes;
        }

        public void setCount_likes(String count_likes) {
            this.count_likes = count_likes;
        }

        public String getCheck_liked() {
            return check_liked;
        }

        public void setCheck_liked(String check_liked) {
            this.check_liked = check_liked;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getProfile_picture() {
            return profile_picture;
        }

        public void setProfile_picture(String profile_picture) {
            this.profile_picture = profile_picture;
        }


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public List<eg.com.cat.fit100.Model.photos> getPhotos() {
            return photos;
        }

        public void setPhotos(List<eg.com.cat.fit100.Model.photos> photos) {
            this.photos = photos;
        }
    }
}
