package eg.com.cat.fit100.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.gson.Gson;
import com.onesignal.OneSignal;

import eg.com.cat.fit100.Model.User;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.Fragments.EventsFragment;
import eg.com.cat.fit100.View.Fragments.HomeFragment;
import eg.com.cat.fit100.View.Fragments.LoginFragment;
import eg.com.cat.fit100.View.Fragments.NewsFeedFragment;
import eg.com.cat.fit100.View.Fragments.NotificationFragment;
import eg.com.cat.fit100.View.Fragments.OffersFragment;

public class MainActivity extends AppCompatActivity {

    public static User currentuser = new User();
    private static Context context;
    String sessionId = "";

    public static Context getContext() {
        return context;
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();

        setupOneSignal();

        SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);


        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int value = extras.getInt("EXTRA_SESSION_ID");
            sessionId = String.valueOf(value);

            Gson gson = new Gson();
            String json = mPrefs.getString("MyObject", "");
            currentuser = gson.fromJson(json, User.class);


        }

        if (sessionId.equals("0")) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new HomeFragment())
                    .commit();
        } else if (sessionId.equals("1")) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new OffersFragment())
                    .commit();
        } else if (sessionId.equals("2")) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new EventsFragment())
                    .commit();
        } else if (sessionId.equals("3")) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new NewsFeedFragment())
                    .commit();
        } else if (sessionId.equals("4")) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new NotificationFragment())
                    .commit();
        } else
            initLogin();
        //    gotomain();


    }

    private void gotomain() {
        try {
            Intent k = new Intent(MainActivity.this, NavigationActivity.class);
            startActivity(k);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initLogin() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new LoginFragment())
                .commit();
    }

    private void setupOneSignal() {

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {

        /*for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }*/

        /*getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, new HomeFragment())
                .commit();*/


        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }
}
