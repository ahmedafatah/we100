package eg.com.cat.fit100.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import eg.com.cat.fit100.R;
import eg.com.cat.fit100.View.Fragments.ActivityChooser;

public class ActivityActivity extends AppCompatActivity {

    ImageView bck;
    ImageView home, offers, events, notification, btn_newsfeed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity);

        setButtons();
        init();

    }

    private void setButtons() {

        home = findViewById(R.id.btn_home);
        offers = findViewById(R.id.btn_offers);
        events = findViewById(R.id.btn_events);
        notification = findViewById(R.id.btn_notification);
        bck = findViewById(R.id.bck);
        btn_newsfeed = findViewById(R.id.btn_newsfeed);


        bck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ActivityActivity.super.onBackPressed();

            }
        });

        btn_newsfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 3);
                startActivity(intent);
            }
        });

        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 1);
                startActivity(intent);
            }
        });

        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 2);
                startActivity(intent);
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 4);
                startActivity(intent);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("EXTRA_SESSION_ID", 0);
                startActivity(intent);
            }
        });


    }


    private void init() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activitycontainer, new ActivityChooser())
                .commit();
    }

}
