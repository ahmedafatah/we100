package eg.com.cat.fit100.Presenter.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import eg.com.cat.fit100.Model.CurrentDayResponseModel;
import eg.com.cat.fit100.R;


public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ExerciseHolder> {
    CurrentDayResponseModel exerciseResponseModel;
    private Context context;

    public ExerciseAdapter(CurrentDayResponseModel exerciseResponseModel, Context context) {
        this.exerciseResponseModel = exerciseResponseModel;
        this.context = context;
    }


    @NonNull
    @Override
    public ExerciseAdapter.ExerciseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ExerciseAdapter.ExerciseHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_exercise_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ExerciseAdapter.ExerciseHolder holder, int position) {

        holder.offerTitle.setText(exerciseResponseModel.getData().get(0).getExercises().get(position).getTitle());
        //holder.offerImage.setText(offersResponseModel.getData().get(position).getTitle());
        holder.offerdescription.setText(exerciseResponseModel.getData().get(0).getExercises().get(position).getBody());


       /* Picasso.get().load(MAIN_PICTURES_URL +
                exerciseResponseModel.getDATA().get(position).getPhotos().get(0).getPath())
                .fit()
                .centerCrop()
                .into(holder.offerImage);*/


    }

    @Override
    public int getItemCount() {
        return exerciseResponseModel.getData().get(0).getExercises().size();
    }

    public class ExerciseHolder extends RecyclerView.ViewHolder {

        TextView offerTitle, offerdescription;
        ImageView moreinfooffers, offerImage;
        LinearLayout Listen;
        View view;

        public ExerciseHolder(View itemView) {
            super(itemView);

            offerTitle = (TextView) itemView.findViewById(R.id.offerTitle);
            offerdescription = (TextView) itemView.findViewById(R.id.offerdescription);
            //        moreinfooffers = itemView.findViewById(R.id.moreinfooffers);
            //      offerImage = itemView.findViewById(R.id.offerImage);

          /*  moreinfooffers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext() , "no more info " , Toast.LENGTH_LONG).show();
                }
            });
*/
        }
    }
}
