package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import eg.com.cat.fit100.Model.Notifications_item;
import eg.com.cat.fit100.Presenter.Adapters.Notifications_adapter;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotificationFragment extends Fragment {

    ImageView home, offers, events, notification, bck;

    TextView myempty;

    SwipeRefreshLayout mSwipeRefreshLayout;

    Notifications_item notifications_item;

    RecyclerView rcvOffers;
    Notifications_adapter adapter;
    ProgressDialog progress;
    private Handler handler;


    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);


        home = view.findViewById(R.id.btn_home);
        offers = view.findViewById(R.id.btn_offers);
        events = view.findViewById(R.id.btn_events);
        notification = view.findViewById(R.id.btn_notification);
        bck = view.findViewById(R.id.bck);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new OffersFragment())
                        .commit();
            }
        });

        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new EventsFragment())
                        .commit();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });

        bck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });


        mSwipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        rcvOffers = view.findViewById(R.id.rcvnotification);
        myempty = view.findViewById(R.id.mytxtEmpty);

        loadNotifications();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                loadNotifications();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        return view;
    }

    public void loadNotifications() {

        progress.show();


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };

        new Thread() {
            public void run() {

                Webservice.getInstance().getApi().getNotifications().enqueue(new Callback<Notifications_item>() {
                    @Override
                    public void onResponse(Call<Notifications_item> call, Response<Notifications_item> response) {
                        if (response.isSuccessful()) {

                            notifications_item = response.body();
                            progress.dismiss();

                            if (notifications_item.getData().size() <= 0 )
                                myempty.setVisibility(View.VISIBLE);
                            else
                                myempty.setVisibility(View.GONE);

                            setOffersData();


                        } else {
                            Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            myempty.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<Notifications_item> call, Throwable t) {
                        Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                        progress.dismiss();
                    }
                });

            }

        }.start();


    }

    private void setOffersData() {

        adapter = new Notifications_adapter(notifications_item, getActivity());
        rcvOffers.setAdapter(adapter);
        rcvOffers.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rcvOffers.setLayoutManager(new LinearLayoutManager(getActivity()));

    }
}
