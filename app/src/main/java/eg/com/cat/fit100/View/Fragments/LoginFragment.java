package eg.com.cat.fit100.View.Fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import eg.com.cat.fit100.Model.LoginResponse;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static eg.com.cat.fit100.View.MainActivity.currentuser;


public class LoginFragment extends Fragment {

    EditText email, password;
    Button login, register;
    TextView forgetPassword;
    ProgressDialog progress;
    CheckBox checkRemeber;
    String idcheck;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        email = (EditText) view.findViewById(R.id.input_username);
        password = (EditText) view.findViewById(R.id.input_password);
        login = (Button) view.findViewById(R.id.btn_login);
        register = (Button) view.findViewById(R.id.btn_register);
        checkRemeber = view.findViewById(R.id.chckRemember);
        forgetPassword = (TextView) view.findViewById(R.id.link_forgetpassword);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog


        loginPreferences = getActivity().getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();


        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            email.setText(loginPreferences.getString("username", ""));
            password.setText(loginPreferences.getString("password", ""));
            checkRemeber.setChecked(true);
        }


        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login.startAnimation(buttonClick);

                login(email.getText().toString().trim(), password.getText().toString().trim());
            }
        });


        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity

                register.startAnimation(buttonClick);

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new RegisterFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });


        forgetPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                forgetPassword.startAnimation(buttonClick);

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new ForgetPasswordFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });


        return view;
    }

    public void login(String user, final String pass) {

        final HashMap<String, String> data = new HashMap<>();

        data.put("email", user);
        data.put("password", pass);


        if (user.isEmpty() || pass.isEmpty())
            Toast.makeText(getActivity(), "Please enter your credentials correctly ", Toast.LENGTH_LONG).show();

        else {


            progress.show();

            Webservice.getInstance().getApi().Login(data).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("data"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }

                        progress.dismiss();


                    } else {


                        final int userId = response.body().getData().id;
                        final String userIdString = Integer.toString(userId);


                        Toast.makeText(getActivity(), "Data Get Successfully", Toast.LENGTH_LONG).show();
                        currentuser.setName(response.body().data.name);
                        currentuser.setCountry(response.body().data.country);
                        currentuser.setId(String.valueOf(response.body().data.id));
                        currentuser.setUsername(response.body().data.username);
                        currentuser.setEmail(response.body().data.email);
                        currentuser.setGender(response.body().data.gender);
                        currentuser.setHeight(response.body().data.height);
                        currentuser.setWeight(response.body().data.weight);
                        currentuser.setBmi(response.body().data.bmi);
                        currentuser.setPhotos(response.body().data.photos);


                        SharedPreferences mPrefs = getActivity().getPreferences(MODE_PRIVATE);

                        SharedPreferences.Editor prefsEditor = mPrefs.edit();
                        Gson gson = new Gson();
                        String json = gson.toJson(currentuser); // myObject - instance of MyObject
                        prefsEditor.putString("MyObject", json);
                        prefsEditor.commit();

                        if (checkRemeber.isChecked()) {
                            loginPrefsEditor.putBoolean("saveLogin", true);
                            loginPrefsEditor.putString("username", response.body().data.email);
                            loginPrefsEditor.putString("password", pass);
                            loginPrefsEditor.commit();
                        } else {
                            loginPrefsEditor.clear();
                            loginPrefsEditor.commit();
                        }


                        progress.dismiss();


                        if (response.body().getData().program_id == null) {
                            goToEditProfile();
                        } else {
                            gotoHome();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                    Log.e("login", "onFailure: ", t);
                    progress.dismiss();

                }
            });

        }
    }

    private void goToEditProfile() {

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new DietFragment())
                .commit();

    }

    private void gotoHome() {

       /* try {
            Intent k = new Intent(getActivity(), NavigationActivity.class);
            startActivity(k);
        } catch(Exception e) {
            e.printStackTrace();
        }*/

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .commit();


    }


}
