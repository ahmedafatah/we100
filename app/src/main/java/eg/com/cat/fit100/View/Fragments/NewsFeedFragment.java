package eg.com.cat.fit100.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import eg.com.cat.fit100.Model.PostsResponseModel;
import eg.com.cat.fit100.Presenter.Adapters.NewsFeedAdapter;
import eg.com.cat.fit100.Presenter.FileUtils;
import eg.com.cat.fit100.Presenter.SwipeController;
import eg.com.cat.fit100.Presenter.SwipeControllerActions;
import eg.com.cat.fit100.R;
import eg.com.cat.fit100.WebServices.Webservice;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static eg.com.cat.fit100.View.MainActivity.currentuser;


public class NewsFeedFragment extends Fragment {

    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
     PostsResponseModel postsResponseModel;
    public static PostsResponseModel localPosts;
    ImageView bck, home, offers, events, notification, tobeUploadpic, myprofilepicture;
    TextView addpost_edittext;
    Button sendpost;
    String strBase64;
    RecyclerView rcvOffers;
    NewsFeedAdapter adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Uri uri;
    private Handler handler;

    TextView myempty;
    ProgressDialog progress;

    SwipeController swipeController = null;

    public NewsFeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_feed, container, false);

       /* home = view.findViewById(R.id.btn_home);
        offers = view.findViewById(R.id.btn_offers);
        events = view.findViewById(R.id.btn_events);
        notification = view.findViewById(R.id.btn_notification);*/
        bck = view.findViewById(R.id.bck);
        addpost_edittext = view.findViewById(R.id.addpost_edittext);
        rcvOffers = view.findViewById(R.id.postsDetails);
        mSwipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);

        myempty = view.findViewById(R.id.mytxtEmpty);

        progress = new ProgressDialog(getActivity());

        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        /*offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new OffersFragment())
                        .commit();
            }
        });


        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new EventsFragment())
                        .commit();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new NotificationFragment())
                        .commit();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });
*/
        bck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, new HomeFragment())
                        .commit();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getPosts();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


        addpost_edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new newPostFragment())
                        .addToBackStack(null)
                        .commit();

            }
        });

        getPosts();

        return view;
    }


    private void getPosts() {

        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };


        new Thread() {
            public void run() {

                Webservice.getInstance().getApi().getposts(currentuser.getId()).enqueue(new Callback<PostsResponseModel>() {
                    @Override
                    public void onResponse(Call<PostsResponseModel> call, Response<PostsResponseModel> response) {
                        if (response.isSuccessful()) {

                            progress.dismiss();
                            postsResponseModel = response.body();

                            if (postsResponseModel.getDATA().size() <= 0)
                                myempty.setVisibility(View.VISIBLE);
                            else
                                myempty.setVisibility(View.GONE);

                            localPosts = postsResponseModel;
                            setOffersData();

                        } else {
                            Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            myempty.setVisibility(View.VISIBLE);

                        }
                    }

                    @Override
                    public void onFailure(Call<PostsResponseModel> call, Throwable t) {
                        Toast.makeText(getActivity(), "failure , check your connection", Toast.LENGTH_LONG).show();
                        progress.dismiss();

                    }
                });
            }
        }.start();

    }

    private void setOffersData() {
        adapter = new NewsFeedAdapter(postsResponseModel, getActivity());
        rcvOffers.setAdapter(adapter);
        rcvOffers.setLayoutManager(new LinearLayoutManager(getActivity()));

        swipeController = new SwipeController(postsResponseModel , new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                
                removepost(currentuser.getId() , postsResponseModel.getDATA().get(position).getId());

                adapter.postsResponseModel.getDATA().remove(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, adapter.getItemCount());


            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(rcvOffers);


        rcvOffers.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });


        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));


        rcvOffers.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.HORIZONTAL));

        rcvOffers.setHasFixedSize(true);
    }

    private void removepost(final String id, final String postId) {

        progress.show();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }

        };

        new Thread() {
            public void run() {

                HashMap<String, String> data = new HashMap<>();

                    data.put("user_id", id);

                    Response<Object> response = null;
                    try {
                        response = Webservice.getInstance().getApi().delete_post(postId , data).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                        } catch (Exception e) {
                        }

                        progress.dismiss();

                    } else {
                        progress.dismiss();

                    }
            }


        }.start();

    }

    private void uploadPicture() {

        if (EasyPermissions.hasPermissions(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
            openGalleryIntent.setType("image/*");
            startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(getActivity(), "Gallery Permission",
                    READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            uri = data.getData();
            uploadFile(uri);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void uploadFile(Uri fileUri) {

        File file = FileUtils.getFile(getContext(), fileUri);
        final Bitmap selectedImage = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 30, stream);
        byte[] byteArray = stream.toByteArray();
        strBase64 = Base64.encodeToString(byteArray, 0);

        tobeUploadpic.setImageBitmap(selectedImage);

    }




}
